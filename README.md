# Gämbling Blogi
Tämä on  tälläinen aika pitkälle vain kokeilu sivusto, mikä mahdollisesti olisi jokusessa vaiheessa tarkoitus laittaa näkyviin johonkin oikeaan osoitteeseen, eikä tuohon Netlifyn generoimaan auto url osoitteeseen.

## Sivuston rakenne
Tää sivusto on rakennettu staattiselle sivu genellä, joka on tosi kätevä tooli sivun kuin sivun tekemiseen. Toki tää nyt hieman enemmän vaatii kuin ehkä sellaisen perussivun tekeminen jollain normi jutulla, kun tässä on hieman näitä kikkailuja mitä pitää osata.

### Hyötyjä staattisesta sivusta
Staattista sivua on helpompi ylläpitää verrattuna johonkin CMS ohjelman ja tietokantaan. Tää helpottaa eritoten silloin, kun sivua ei halua tai jaksa ihan koko aikaa monitoroida ja tarkkailla mitä siellä tapahtuu. Eli admin hommat hieman keventyy tällä meiningillä.

### Mikä on ReactJS?
Tämä sivusto käyttää uutta modernia React javascript kirjastoa, jota kannattaa alkaa opetella tarkemmin. 

### Tskekkaa tänä repon demo saitti
Jos haluat käydä katsomassa tämän [rizki sivuston](http://money-lender-gyroscope-88031.netlify.com/), niin se on livenä Netlifyssä. Tsekkaa saitti tuosta aikaisemmasta linkistä

## Miksi tehdä sivusto käsittelemään pelaamista?
Mää digaan pelailla erilaisilla nettikasinoilla, joten kelasin laittaa pystyyn tän sivuston, missä minä sitten jaan erilaisia vinkkejä ja muita tipsejä. Tämä vinkkien jakaminen on erinomaisesti sopiva juuri monenlaisille pelaajille, mutta eritoten sellaisille, jotka haluavat näitä hyviä vinkkejä seurata tarkemmin. Gämbläys kandee tehdä aina mahollisimman hyvin, sillä silloin on mahikset päästä parhaisiin tuloksiin 100% varmuudella.

Kun pelaa bonuksia, niin kannattaa pitää fokus itse päämäärässä, eli sen bonuksen graindaamisessa eikä missään muussa sellaisessa, mikä ei tätä tarkoitusta suoranaisesti tue. 

## Sivusto by Tove
Tämä Tove on jokseenkin kokenut devaaja kettu Suomesta, joka tykkää testata erinäisiä uusia asioita, kuten ReactJS:ää. Tää saitti on niin ikää tälläinen Toven leikkipaikka, ei niinkään vakavasti otettava sivusto. Katotaan jos homma ottaa siivet ja lähtee lentoon, niin sitten tämä voi sellainen juttu, jota alkaa vakavemmin painaa. Tällä hetkellä tämä on vain lähinnä kunhan opettelen mitä tätä hommaa pystyy rulata tämän kautta.